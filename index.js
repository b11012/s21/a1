let users = ["Dwayne Johnson","Steve Austin","Kurt Angle","Dave Bautista"];

console.log("Original Array:")
console.log(users);

/*
    1. Create a function which is able to receive a single argument and add the input at the end of the users array.
        -function should be able to receive a single argument.
        -add the input data at the end of the array.
        -The function should not be able to return data.
        -invoke and add an argument to be passed in the function.
        -log the users array in the console.

*/
function addItem(item){
    users[users.length] = item;
}

addItem("John Cena");
console.log(users)
/*
    2. Create function which is able to receive an index number as a single argument return the item accessed by its index.
        -function should be able to receive a single argument.
        -return the item accessed by the index.
        -Create a global variable called outside of the function called itemFound and store the value returned by the function in it.
        -log the itemFound variable in the console.

*/
function getItemByIndex(index){
    return users[index]
}

let itemFound = getItemByIndex(2);
console.log(itemFound);

/*
    3. Create function which is able to delete the last item in the array and return the deleted item.
        -Create a function scoped variable to store the last item in the users array.
        -Shorten the length of the array.
        -return the last item in the array.
        -Create a global scoped variable outside of the function and store the result of the function.
        -log the global scoped variable in the console.

*/
function deleteItem(){
    let deleted = users[users.length-1]
    users.length--;
    return deleted;
}

let deletedItem = deleteItem();
console.log(deletedItem);
console.log(users);


/*
    4. Create function which is able to update a specific item in the array by its index.
        -Function should be able to receive 2 arguments, the update and the index number.
        -First, access and locate the item by its index then re-assign the item with the update.
        -This function should not have a return.
        -Invoke the function and add the update and index number as arguments.
        -Outside of the function, Log the users array in the console.

*/

function updateItemByIndex(update,index){
    users[index] = update;
}

updateItemByIndex("Triple H",3);
console.log(users);

/*
    5. Create function which is able to delete all items in the array.
        -You can modify/set the length of the array.
        -The function should not return anything.
        -Invoke the function.
        -Outside of the function, Log the users array in the console.

*/

function deleteAll(){
    //users.length = 0;
    users = [];
}

// deleteAll();
console.log(users)


/*
    6. Create a function which is able to check if the array is empty.
        -Check if the length of the users array is greater than 0.
            -If it is, return false.
            -If it is not, return true.
        -Create a global variable called outside of the function  called isUsersEmpty and store the returned value from the function.
        -log the isUsersEmpty variable in the console.

*/
function isEmpty(){
    if(users.length > 0){
        return false
    } else {
        return true
    }
}
let isUsersEmpty = isEmpty();
console.log(isUsersEmpty);

/*

    Stretch Goal:

    Create a function which is able to get the index number of an item.
        -The function should be able to receive a single argument. The data input will be matched against each item in the array, if the input matched an item in the array we will return the current index number else we will return -1
        -Add a for loop to iterate over each item in the array.
        -in the for loop, add an if statement to check if the input item is the same as array item currently being iterated or looped.
        -if it matches, return the value of the index or the value of the variable used as a counter for the loop.
        -add an Else statement to return -1, if otherwise.
        -Create a global variable called outside of the function indexOfItem and store the returned value of the function.
        -log the value of the indexOfItem in the console.
*/

function getIndexByItem(item){

    for(let index = 0; index < users.length; index++){

        if(item === users[index]){
            return index;
       
        }
    }
    return -1
}

let indexOfItem = getIndexByItem("Steve Austin");

console.log(indexOfItem);

// note: updates
